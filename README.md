# Application is based on a Create-React-App typescript template

Main logic code resides in src/Model

Component code resides in src/Components

The entry component is ATM component which is instantiated in App.tsx and the main logic file is ATM.ts and the IWithdrawable interface in IWithdrawable.ts file.

Simple test of the ATM logic file is done in the App.test.tsx

to start the application run

`npm install`
and after
`npm start`

to run tests use command
`npm test`
