import { IValuable } from "./IValuable";

export interface IPayout {
	readonly cash: IValuable[];
	readonly amount: number;
}
