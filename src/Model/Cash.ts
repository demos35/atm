import { ISubtractableValuable } from "./ISubtractableValuable";
import { IValuable } from "./IValuable";

export class Cash implements IValuable {
	protected _count: number = 0;
	protected _value: number = 0;

	constructor(count: number, value: number) {
		this._count = count;
		this._value = value;
	}

	public get value() {
		return this._value;
	}

	public get count() {
		return this._count;
	}

	public get amount() {
		return this._count * this._value;
	}
}

export class PayoutBoxCash extends Cash implements ISubtractableValuable {
	public getWithdrawableAmount(amount: number): number {
		const countNeeded = Math.floor(amount / this._value);
		if (countNeeded <= this._count) {
			return countNeeded * this._value;
		} else {
			return this._count * this._value;
		}
	}

	/**
	 *
	 * @param amount
	 * @returns the requested amount if available, otherwise the max amount available
	 */
	public withdraw(amount: number): IValuable {
		const countNeeded = Math.floor(amount / this._value);

		if (countNeeded <= this._count) {
			this._count -= countNeeded;
			return new Cash(countNeeded, this._value);
		} else {
			const available = this._count;
			this._count = 0;
			return new Cash(available, this._value);
		}
	}
}
