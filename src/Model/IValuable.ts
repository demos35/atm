export interface IValuable {
	readonly value: number;
	readonly count: number;
	readonly amount: number;
}
