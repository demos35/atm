import { IValuable } from "./IValuable";
import { Payout } from "./Payout";
import { IPayoutBox } from "./PayoutBox";
import { IWithdrawable } from "./IWithdrawable";

export class ATM implements IWithdrawable {
	private valueBoxMap: Map<number, IPayoutBox>;
	private availableValues: number[];
	constructor(payoutBoxes: IPayoutBox[]) {
		this.valueBoxMap = new Map<number, IPayoutBox>();
		payoutBoxes.forEach((payoutBox) => {
			payoutBox.getAvailableValuables().forEach((value) => {
				this.valueBoxMap.set(value, payoutBox);
			});
		});
		this.availableValues = Array.from(this.valueBoxMap.keys()).sort((value1, value2) => value2 - value1);
	}

	private getPayoutBoxByValue(value: number) {
		const cash = this.valueBoxMap.get(value);
		if (!cash) {
			throw new Error("PayoutBox with value does not exist");
		}
		return cash;
	}

	private isCashAvailable(amount: number): boolean {
		for (let i = 0; i < this.availableValues.length; i++) {
			amount -= this.getPayoutBoxByValue(this.availableValues[i]).getAvailableAmountByValue(
				amount,
				this.availableValues[i]
			);
		}

		return amount <= 0;
	}

	public withdraw(amount: number): Payout {
		if (!this.isCashAvailable(amount)) {
			throw new Error("Cash not available");
		}

		const allCash: IValuable[] = [];
		for (let i = 0; i < this.availableValues.length; i++) {
			const cash = this.getPayoutBoxByValue(this.availableValues[i]).withDraw(amount, this.availableValues[i]);
			amount -= cash.amount;
			if (cash.count > 0) {
				allCash.push(cash);
			}
		}
		return new Payout(allCash);
	}
}
