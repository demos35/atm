import { IPayout } from "./IPayout";
import { IValuable } from "./IValuable";

export class Payout implements IPayout {
	private _cash: IValuable[];

	constructor(cash: IValuable[]) {
		this._cash = cash;
	}

	public get cash() {
		return this._cash;
	}

	public get amount() {
		return this.calculateAmount();
	}

	private calculateAmount(): number {
		const cashReducer = (total: number, current: IValuable) => {
			return total + current.amount;
		};

		return this._cash.reduce(cashReducer, 0);
	}
}
