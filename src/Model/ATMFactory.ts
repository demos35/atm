import { ATM } from "./ATM";
import { PayoutBoxCash } from "./Cash";
import { ISubtractableValuable } from "./ISubtractableValuable";
import { PayoutBox } from "./PayoutBox";

export type ATMOptions = {
	ones?: number;
	twos?: number;
	fives?: number;
	tens?: number;
	twenties?: number;
	fifties?: number;
	hundreds?: number;
	twohundreds?: number;
	fivehundreds?: number;
	thousands?: number;
};
export class ATMFactory {
	public static instantiateATM(options: ATMOptions) {
		const smallCoins: ISubtractableValuable[] = [];
		const bigCoins: ISubtractableValuable[] = [];
		const notes: ISubtractableValuable[] = [];

		if (options.ones !== undefined) {
			smallCoins.push(new PayoutBoxCash(options.ones, 1));
		}
		if (options.twos !== undefined) {
			smallCoins.push(new PayoutBoxCash(options.twos, 2));
		}
		if (options.fives !== undefined) {
			bigCoins.push(new PayoutBoxCash(options.fives, 5));
		}
		if (options.tens !== undefined) {
			smallCoins.push(new PayoutBoxCash(options.tens, 10));
		}
		if (options.twenties !== undefined) {
			bigCoins.push(new PayoutBoxCash(options.twenties, 20));
		}
		if (options.fifties !== undefined) {
			notes.push(new PayoutBoxCash(options.fifties, 50));
		}
		if (options.hundreds !== undefined) {
			notes.push(new PayoutBoxCash(options.hundreds, 100));
		}
		if (options.twohundreds !== undefined) {
			notes.push(new PayoutBoxCash(options.twohundreds, 200));
		}
		if (options.fivehundreds !== undefined) {
			notes.push(new PayoutBoxCash(options.fivehundreds, 500));
		}
		if (options.thousands !== undefined) {
			notes.push(new PayoutBoxCash(options.thousands, 1000));
		}
		return new ATM([new PayoutBox(smallCoins), new PayoutBox(bigCoins), new PayoutBox(notes)]);
	}
}
