import { IValuable } from "./IValuable";

export interface ISubtractableValuable extends IValuable {
	getWithdrawableAmount(amount: number): number;
	withdraw(amount: number): IValuable;
}
