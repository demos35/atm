import { ISubtractableValuable } from "./ISubtractableValuable";
import { IValuable } from "./IValuable";

export interface IPayoutBox {
	getAvailableAmountByValue(amount: number, value: number): number;
	getAvailableValuables(): number[];
	withDraw(amount: number, value: number): IValuable;
}
export class PayoutBox implements IPayoutBox {
	private _content: ISubtractableValuable[];

	constructor(content: ISubtractableValuable[]) {
		this._content = content.sort((a, b) => b.value - a.value);
	}

	private getValuableByValue(value: number): ISubtractableValuable {
		const cash = this._content.find((c) => c.value === value);
		if (!cash) {
			throw new Error("value not available in this PayoutBox");
		}
		return cash;
	}

	public getAvailableAmountByValue(amount: number, value: number): number {
		return this.getValuableByValue(value).getWithdrawableAmount(amount);
	}

	public getAvailableValuables(): number[] {
		return this._content.map((cash) => cash.value);
	}

	public withDraw(amount: number, value: number): IValuable {
		const cash = this.getValuableByValue(value).withdraw(amount);
		return cash;
	}
}
