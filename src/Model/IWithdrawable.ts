import { Payout } from "./Payout";

export interface IWithdrawable {
	withdraw(amount: number): Payout;
}
