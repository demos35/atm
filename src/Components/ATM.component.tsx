import React, { useState, useRef } from "react";
import { IPayout } from "../Model/IPayout";
import { Button } from "./Button.component";
import { Input } from "./Input.component";
import { PayoutComponent } from "./Payout.component";
import { Warning } from "./Warning.component";
import { ATMFactory } from "../Model/ATMFactory";
import { IWithdrawable } from "../Model/IWithdrawable";

export const ATMContainer = () => {
	const [atmLogic] = useState<IWithdrawable>(
		ATMFactory.instantiateATM({
			ones: 100,
			twos: 100,
			fives: 100,
			tens: 100,
			twenties: 100,
			fifties: 100,
			hundreds: 100,
			twohundreds: 100,
			fivehundreds: 100,
			thousands: 100,
		})
	);

	const [error, setError] = useState<string | null>(null);
	const [input, setInput] = useState("");
	const [payout, setPayout] = useState<IPayout | null>(null);
	const inputRef = useRef<HTMLInputElement>();

	const handleInputChange = (e: React.FormEvent<HTMLInputElement>) => {
		if (isNaN(+e.currentTarget.value)) {
			return;
		}
		setInput(Math.abs(+e.currentTarget.value).toFixed(0));
	};

	const handleClear = () => {
		setInput("");
		setPayout(null);
		setError(null);
		inputRef?.current?.focus();
	};

	const handleWithdraw = () => {
		if (+input > 0) {
			try {
				const payout = atmLogic.withdraw(+input);
				setPayout(payout);
			} catch (e) {
				setError(e.message);
			}
		}
	};

	return (
		<div
			style={{
				display: "flex",
				flexDirection: "column",
				alignItems: "center",
				justifyContent: "center",
				width: "100%",
				height: "100%",
			}}
		>
			{error !== null && <Warning>{error}</Warning>}
			<Input innerRef={inputRef} value={input} onChange={handleInputChange} />
			<div
				style={{
					display: "flex",
					flexDirection: "row",
				}}
			>
				<Button disabled={payout != null} onClick={handleWithdraw}>
					Withdraw
				</Button>
				<Button secondary onClick={handleClear}>
					Clear
				</Button>
			</div>
			{payout !== null && <PayoutComponent payout={payout} />}
		</div>
	);
};
