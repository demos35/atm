import Styled from "styled-components";
import { IPayout } from "../Model/IPayout";
export const PayoutComponent = ({ payout }: { payout: IPayout }) => {
	console.log({ payout });
	const NOTES_THRESHOLD = 50;
	return (
		<PayoutStyle>
			{payout.cash.map(({ value, count }) => {
				return (
					<PayoutElement>
						{value >= NOTES_THRESHOLD ? <Note>{value}</Note> : <Coin>{value}</Coin>}
						<span>{`x${count}`}</span>
					</PayoutElement>
				);
			})}
		</PayoutStyle>
	);
};

const PayoutStyle = Styled.div`
  display: flex;
  flex-direction: column;
  width: 100px;
`;

const PayoutElement = Styled.div`
  margin-bottom: 16px;
  display: flex;
  justify-content: space-between;
  align-items:center;
  flex-direction: row;
`;

const Note = Styled.div`
  border: 1px solid #000;
  background-color: #85bb65;
  color: #fff;
  height: 30px;
  width: 60px;
  display: flex;
  align-items: center;
  justify-content:center;
`;

const Coin = Styled.div`
  background-color: #FFD700;
  border: 1px solid #000;
  height: 30px;
  width: 30px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content:center
`;
