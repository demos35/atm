import Styled from "styled-components";

export const Warning = ({ children }: any) => <WarningStyle>{children}</WarningStyle>;

const WarningStyle = Styled.div`
    font-size: 24px;
    color: red;
    font-weight: bold;
`;
