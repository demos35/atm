import Styled from "styled-components";

export const Button = ({ children, onClick = () => null, secondary = false, disabled = false }: any) => {
	return (
		<ButtonStyle disabled={disabled} secondary={secondary} onClick={onClick}>
			{children}
		</ButtonStyle>
	);
};

const ButtonStyle = Styled.button<{ secondary: boolean }>`
    border: none;
    border-radius: 4px;
    padding: 8px 16px;
    margin: 8px 8px;
    color: #fff;
    cursor: pointer;
    background-color: ${(props: any) => (props.secondary ? "#7d8580" : "#36bf71")};
    &:hover {
      background-color: ${(props: any) => (props.secondary ? "#414643" : "#1f5838")};
    }
    &:disabled {
      opacity: 0.5;
    }
`;
