import Styled from "styled-components";

export const Input = ({ onChange, value, innerRef }: any) => {
	return <InputStyle ref={innerRef} type="number" value={value} onChange={onChange} />;
};

const InputStyle = Styled.input`
text-align: center;
  padding: 16px;
  font-size: 32px;
  letter-spacing:1px;
  
`;
