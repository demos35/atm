// import React from "react";
// import { render, screen } from "@testing-library/react";
// import App from "./App";
import { PayoutBoxCash } from "./Model/Cash";

import { PayoutBox } from "./Model/PayoutBox";
import { ATM } from "./Model/ATM";
import { ATMFactory } from "./Model/ATMFactory";

// test("renders learn react link", () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

test("limit", () => {
	const ones = new PayoutBoxCash(100, 1);
	const smallCoinPayoutBox = new PayoutBox([ones]);
	const atm = new ATM([smallCoinPayoutBox]);

	const payout = atm.withdraw(50);
	expect(payout.amount).toBe(50);
	expect(payout.cash.length).toBe(1);
	expect(payout.cash[0].value).toBe(1);
});

test("payout should contain the least amount of notes and coins", () => {
	// const atm = new ATM([smallCoinPayoutBox, bigCoinPayoutBox, notesPayoutBox]);
	const atm = ATMFactory.instantiateATM({
		ones: 100,
		twos: 100,
		fives: 100,
		tens: 100,
		twenties: 100,
		fifties: 100,
		hundreds: 100,
		twohundreds: 100,
		fivehundreds: 100,
		thousands: 100,
	});
	const payout = atm.withdraw(578);
	expect(payout.amount).toBe(578);
	expect(payout.cash.find((c) => c.value === 1)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 2)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 5)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 10)).toBe(undefined);
	expect(payout.cash.find((c) => c.value === 20)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 50)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 100)).toBe(undefined);
	expect(payout.cash.find((c) => c.value === 200)).toBe(undefined);
	expect(payout.cash.find((c) => c.value === 500)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 1000)).toBe(undefined);
});

test("payout should contain the least amount of notes and coins, even when a 500 note is empty", () => {
	// const atm = new ATM([smallCoinPayoutBox, bigCoinPayoutBox, notesPayoutBox]);
	const atm = ATMFactory.instantiateATM({
		ones: 100,
		twos: 100,
		fives: 100,
		tens: 100,
		twenties: 100,
		fifties: 100,
		hundreds: 100,
		twohundreds: 100,
		fivehundreds: 0,
		thousands: 100,
	});
	const payout = atm.withdraw(578);
	expect(payout.amount).toBe(578);
	expect(payout.cash.find((c) => c.value === 1)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 2)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 5)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 10)).toBe(undefined);
	expect(payout.cash.find((c) => c.value === 20)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 50)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 100)?.count).toBe(1);
	expect(payout.cash.find((c) => c.value === 200)?.count).toBe(2);
	expect(payout.cash.find((c) => c.value === 500)).toBe(undefined);
	expect(payout.cash.find((c) => c.value === 1000)).toBe(undefined);
});

test("atm should throw error when trying to withdraw more than available", () => {
	// const atm = new ATM([smallCoinPayoutBox, bigCoinPayoutBox, notesPayoutBox]);
	const atm = ATMFactory.instantiateATM({
		ones: 1,
		twos: 1,
		fives: 1,
		tens: 1,
		twenties: 1,
		fifties: 1,
		hundreds: 1,
		twohundreds: 1,
		fivehundreds: 1,
		thousands: 1,
	});
	expect(() => atm.withdraw(1889)).toThrowError("Cash not available");
});
