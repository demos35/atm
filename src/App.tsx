import "./App.css";
import { ATMContainer } from "./Components/ATM.component";

function App() {
	return (
		<div className="App">
			<ATMContainer />
		</div>
	);
}

export default App;
